class Contact {
    constructor(firstname, lastname)
    {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    displayContact() {
       console.log(`Nom: ${this.lastname}, prénom: ${this.firstname}`);
    }

}
const contacts = [{lastname: "Lévisse", firstname: "Carole"}, {lastname: "Nelsonne", firstname: "Mélodie"}]

const menu = () => {
    console.log("Bienvenue dans le gestionnaire des contacts");
    console.log("1 : Lister les contacts");
    console.log("2 : Ajouter un contact");
    console.log("0 : Quitter");
}
menu();

const listAllContacts = () => {
    console.log("\nVoici la liste de tous les contacts\n")
    contacts.forEach(item => {
        const contact = new Contact(item.lastname, item.firstname);
        console.log(contact.displayContact());
    })
    choiceOption();
}
const addNewContact = () => 
{
    let last =  prompt("Insérer le nom: ");
    let first =  prompt("Insérer le prénom: ");
    contacts.push({lastname: first, firstname: last});
    console.log("Le nouveau contact a été bien ajouté.");
    choiceOption();
}


const choiceOption = () => {
   const option =  Number(prompt("Choisir une option:"));
  while(option !== 0)
  {
      if(option === 1)
      {
        listAllContacts();  
        break;
      }
      else
      {
        if(option === 2) 
        {
            addNewContact();
            break;
        }
      }
  }
}
choiceOption();
